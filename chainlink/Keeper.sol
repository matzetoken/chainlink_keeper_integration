// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

// KeeperCompatible.sol imports the functions from both ./KeeperBase.sol and
// ./interfaces/KeeperCompatibleInterface.sol
//import "https://github.com/OpenZeppelin/openzeppelin-contracts/blob/v2.5.0/contracts/math/SafeMath.sol";
import "https://github.com/smartcontractkit/chainlink/blob/develop/contracts/src/v0.8/KeeperCompatible.sol";
import "@openzeppelin/contracts/access/Ownable.sol";

interface IBuyBack {
    function buyback() external returns(bool);
    function checkBuyBack() external view  returns(bool shouldBuy, uint256 balance, uint256 supply, uint256 trigger);
}   

contract BuyBackKeeper is KeeperCompatibleInterface, Ownable {

    
    /**
    * 
    */
    address public buyBackAddress;
    /**
    * Use an interval in seconds and a timestamp to slow execution of Upkeep
    */
    uint8 constant private MAX_POOLS = 101; 

    constructor(address buybackAdr) {
        buyBackAddress = buybackAdr;
    }

    function setBuyBackAddress(address inAdr) external onlyOwner returns(address retAdr){
        buyBackAddress = inAdr;
        retAdr = buyBackAddress;
    }

    function checkUpkeep(bytes calldata /* checkData */) external view override returns (bool, bytes memory /* performData */) {
        (bool shouldBuyBack,,,) = IBuyBack(buyBackAddress).checkBuyBack();
        return (shouldBuyBack, bytes(""));
    }

    function performUpkeep(bytes calldata performData) external override {
        IBuyBack(buyBackAddress).buyback();
    }

    
}