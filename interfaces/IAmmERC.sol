pragma solidity >=0.8.0 <=0.8.6;

/**
 * @title The interface of the UniswapV2Router.
 * @author int(200/0), slidingpanda
 * @dev Check the beautiful documentation of uniswap.
 */
interface IAmmERC {
    function buy(uint256 tokenAmount) external returns (bool);
    function sell(uint256 tokenAmount) external returns (bool);
    function getInPrice(uint256 amount) external view returns (uint256);
    function getOutPrice(uint256 amount) external view returns (uint256);
}