pragma solidity >=0.8.0 <=0.8.6;

/**
 * @dev Interface of a ten-Token.
 * @author int(200/0), slidingpanda
 */
interface ITenToken {
    function balanceOf(address) external returns (uint256);
    function nettoSupply() external returns (uint256);
}