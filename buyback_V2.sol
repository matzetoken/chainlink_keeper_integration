pragma solidity >=0.8.0 <=0.8.6;

// import "@openzeppelin/contracts-upgradeable/proxy/utils/Initializable.sol";


import "./interfaces/ISwap.sol";
import "./interfaces/ITenToken.sol";
import "./interfaces/IAmmETH.sol";
import "./interfaces/IAmmERC.sol";
import "@openzeppelin/contracts/security/ReentrancyGuard.sol";
import "@openzeppelin/contracts/access/Ownable.sol";
import "@openzeppelin/contracts/token/ERC20/IERC20.sol";
import "@openzeppelin/contracts/token/ERC20/utils/SafeERC20.sol";


/*
 * @title The almighty Buyback contract.
 * @dev Provides buybacks for the Teneo ecosystem.
 */
contract Buyback is Ownable, ReentrancyGuard {

    using SafeERC20 for IERC20;
   

    address public daoWallet;
    bool public isActive;

    uint8 public percentageOfSupply;
    uint8 public percentageOfBalance;

    address public routerAddress;
    ISwap private router;
    address public ethAmmAddress;
    IAmmETH private ethAMM;
    address public ercAmmAddress;
    IAmmERC private ercAMM;

    address public pegTokenAddress;
    address public midTokenAddress;
    address public outTokenAddress = 0x25B9d4b9535920194c359d2879dB6a1382c2ff26;

    address private wETH = 0xbb4CdB9CBd36B01bD1cBaEBF2De08d9173bc095c;
    address private bUSD = 0xe9e7CEA3DedcA5984780Bafc599bD69ADd087D56;


    address public buyBackAddress;
    IERC20 private _buyBackToken;
    address public sellBackAddress;
    IERC20 private _sellBackToken;

    uint256 constant private MAX_POOLS = type(uint256).max;

    /**
     * @dev Contructor.
     */
    constructor(
            address sellBackAddress_, 
            address buyBackAddress_, 
            address ammETH_,
            address ammERC_,
            address pegToken,
            address midToken,
            address daoWallet_, 
            uint8 percentageOfSupply_,
            uint8 percentageOfBalance_
        ) public {
            sellBackAddress = sellBackAddress_;
            _sellBackToken = IERC20(sellBackAddress);
            buyBackAddress = buyBackAddress_;
            _buyBackToken = IERC20(buyBackAddress);

            ethAmmAddress = ammETH_;
            ethAMM = IAmmETH(ammETH_);
            ercAmmAddress = ammERC_;
            ercAMM = IAmmERC(ammERC_);

            pegTokenAddress = pegToken;
            midTokenAddress = midToken;

            daoWallet = daoWallet_;
            percentageOfSupply = percentageOfSupply_;
            percentageOfBalance = percentageOfBalance_;
            isActive = true;
    }



    /**
     * @dev Sets dao wallet.
     * @param newWallet new wallet
     */
    function setDaoWallet(address newWallet) external onlyOwner returns(address) {
        daoWallet = newWallet;
        return daoWallet;
    }

    /**
     * @dev Sets buyback Token.
     * @param newToken new token
     */
    function setBuyBackToken(address newToken) external onlyOwner {
        buyBackAddress = newToken;
        _buyBackToken = IERC20(newToken);
    }

    /**
     * @dev Sets sell Token.
     * @param newToken new token
     */
    function setSellBackToken(address newToken) external onlyOwner {
        sellBackAddress = newToken;
        _sellBackToken = IERC20(newToken);
    }
    
    /**
     * @dev Activate/Deactivate the possibility to buyback.
     * @return the actual state of _isActive after toggle
     */
    function toggleActive() external onlyOwner returns (bool) {
        isActive = !isActive;
        return isActive;
    }

    /**
     * @dev Sets the middle token of the buyback.
     * @param midToken middle token address of the path for the swap
     */
    function setMidToken(address midToken) external onlyOwner {
        midTokenAddress = midToken;
    }

    /**
     * @dev Sets the UniswapRouterV02 of the buyback.
     * @param newRouter uniswap router address
     */
    function setRouter(address newRouter) external onlyOwner {
        routerAddress = newRouter;
        router = ISwap(newRouter);
    }

    /**
     * @dev Sets the needed percentageOfSupply of the totalSupply to trigger buybacks.
     * @param newpercentageOfSupply multiplier for the percentageOfSupply calculation
     */
    function setPercentageOfSupply(uint8 newpercentageOfSupply) external onlyOwner {
        require(newpercentageOfSupply <= 100, "setpercentageOfSupply: We can't mint... so 100 is the limit");
        percentageOfSupply = newpercentageOfSupply;
    }

    /**
     * @dev Sets the needed percentageOfBalance of the totalSupply to trigger buybacks.
     * @param newpercentageOfBalance multiplier for the percentageOfSupply calculation
     */
    function setPercentageOfBalance(uint8 newpercentageOfBalance) external onlyOwner {
        require(newpercentageOfBalance <= 100, "setPercentageOfBalance: We can't mint... so 100 is the limit");
        percentageOfBalance = newpercentageOfBalance;
    }


    //nonReentrant
    function checkBuyBack() public view  returns(bool shouldBuy, uint256 balance, uint256 supply, uint256 trigger){
        balance = _sellBackToken.balanceOf(daoWallet);
        supply = _sellBackToken.totalSupply();
        trigger = supply * percentageOfSupply / 100;
            
        if (trigger < balance && isActive) {
            shouldBuy = true;
        } else {
            shouldBuy = false;
        }
        
    }

    /**
     * @dev Buys ten-Token back to Teneo if the balance of the buyback contract has a set percentageOfSupply 
     *      of the totalSupply of the given ten-Token.
     */
    function buyback() external returns(bool) {
        (bool haveToBuyBack, uint256 balance, uint256 supply, uint256 trigger) = checkBuyBack();
        if (haveToBuyBack) {
                uint256 deadline = block.timestamp + 20;
                uint256 toBuy = balance * percentageOfBalance / 100;
                _sellBackToken.safeTransferFrom(daoWallet, address(this), toBuy);
                uint256 toBuyAfterTransfer = _sellBackToken.balanceOf(address(this));
                
                ethAMM.sell(toBuyAfterTransfer);

                address[] memory path = new address[](3);
                path[0] = address(pegTokenAddress);
                path[1] = address(midTokenAddress);
                path[2] = address(outTokenAddress);
            
                uint256 currentBalance = address(this).balance;

                router.swapExactETHForTokens{value: currentBalance}(0, path, address(this), deadline);
                
                
            return true;
                
        }  
        return false;
    }



    /**
     * @dev Withdraws the collected Token.
     * @param amount withdraw amount of Token
     * @param to address of the recipient
     * @return transaction status
     */
    function withdrawFunds(address tokenAdr, uint256 amount, address to) onlyOwner external returns (bool) {
        return IERC20(tokenAdr).transfer(to, amount);
    }

    /**
     * @dev Withdraws the ETH of contract balance.
     * @param amount withdraw amount of ETH
     * @param to address of the recipient
     * @return transaction status
     */
    function withdrawETH(address to, uint256 amount) onlyOwner external returns (bool) {
        (bool success, ) = to.call{value: amount}("");
        require(success, "withdrawETH: Failed to transfer the funds.");
        return true;
    }

    receive() external payable {
        // ...
    }

}